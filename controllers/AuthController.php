<?php
/**
 * Created by PhpStorm.
 * User: ciaran
 * Date: 27/05/14
 * Time: 11:37
 */

class AuthController extends GoBaseController {

    //Call the GoBaseController construct
    function __construct()
    {
        parent::__construct();
    }

    public function indexAction()
    {
        $loginModel = new Auth();
        $this->view->render('auth/index');
    }

    public function loginAction()
    {
        $loginModel = new Auth();
        //$loginModel->test();
        $username = $this->getRequest('username');
        $password = $this->getRequest('password');
        $auth = $loginModel->login($username, $password);
        if ($auth === true)
        {
            GoSession::set('loggedIn', true);
            header('location: ../index');
        }
    }

    public function logoutAction()
    {
        GoSession::destroy();
        header('location: ../../auth/');
    }
}