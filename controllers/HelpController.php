<?php
/**
 * Created by PhpStorm.
 * User: ciaran
 * Date: 27/05/14
 * Time: 11:40
 */

class HelpController extends GoBaseController {

    function __construct()
    {
        parent::__construct();
    }

    public function indexAction()
    {
        $this->view->render('help/index');
    }

    public function other($arg = false)
    {
        $this->view->render('help/other');
    }
}