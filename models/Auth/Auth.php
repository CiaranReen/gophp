<?php
/**
 * Created by PhpStorm.
 * User: ciaran
 * Date: 27/05/14
 * Time: 16:05
 */

class Auth extends GoBaseModel {

    public function __construct()
    {
        parent::__construct();
    }

    public function login($username, $password)
    {
        $password = sha1($password);
        $sql = $this->db->prepare('SELECT id FROM user WHERE username = :username AND password = :password');

        $sql->execute(array (
            ':username' => $username,
            ':password' => $password,
        ));

        if ($sql->rowCount() > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public function test()
    {
        $sql = $this->select();
        $sql .= $this->from(array('u' => 'user'), array('u.id', 'u.first_name'));
        $sql .= $this->innerJoin(array('a' => 'address'), 'u.address_id = a.address.id');
        $sql .= $this->where('u.id = ' . 1);
        echo '<pre>'; var_dump($sql); die();
        $query = $this->db->prepare($sql);
        $query->execute();

        $data = $query->fetchAll();

        return $sql;
    }
}