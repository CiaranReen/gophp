<?php
/**
 * Created by PhpStorm.
 * User: ciaran
 * Date: 27/05/14
 * Time: 15:52
 */

class GoDatabase extends PDO {

    public function __construct()
    {
        parent::__construct(''.DB_TYPE.':host='. DB_HOST .';dbname='.DB_NAME.'', ''.DB_USER.'', DB_PASS);
    }
}