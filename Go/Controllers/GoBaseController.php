<?php
/**
 * Created by PhpStorm.
 * User: ciaran
 * Date: 27/05/14
 * Time: 12:06
 * This is the parent controller for the GoPHP framework */

abstract class GoBaseController {

    /**
     * @var
     */
    protected $_request;


    /**
     * Constructor method to link the requested controller to the corresponding model.
     * By design, each model should have the same name as the controller but without anything appended
     * For example, IndexController will be linked to the model Index
     */
    function __construct()
    {
        $this->view = new GoBaseView();

        $class = get_class($this);
        $class = explode('C', $class);
        $file = 'models/'.$class[0].'/'.$class[0].'.php';

        if (file_exists($file))
        {
            require $file;
        }
    }

    /**
     * Get the param from a URL
     * For example http://gophp.co.uk/faq/view/{PARAM}
     */
    public function getParam($url)
    {
        $url = explode('/', $url);
        $param = end($url);

        return $param;
    }

    /**
     * Get the request value and return
     * @param $value
     * @return bool
     */
    public function getRequest($value)
    {
        if ($this->isPost() === true)
        {
            $this->_request = $_POST[$value];
            return $this->_request;
        }
        else if ($this->isGet() === true)
        {
            $this->_request = $_GET[$value];
            return $this->_request;
        }

        return false;
    }

    /**
     * Test the request for a post value
     * @return bool
     */
    public function isPost()
    {
        if (isset($_POST))
        {
            return true;
        }

        return false;
    }

    /**
     * Test the request for a get value
     * @return bool
     */
    public function isGet()
    {
        if (isset($_GET))
        {
            return true;
        }

        return false;
    }
}