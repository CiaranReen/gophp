<?php
/**
 * Created by PhpStorm.
 * User: ciaran
 * Date: 27/05/14
 * Time: 16:01
 */

class GoBaseModel extends GoDatabase {

    /**
     * Construct the Database object for use with all child models
     */
    public function __construct()
    {
        $this->db = new GoDatabase();
    }

    /**
     * Find a row based on the PK.
     * @param $id
     * @param null $table
     * @return mixed
     */
    public function find($id, $table = null)
    {
        $sql = $this->db->prepare('SELECT * FROM ' . $table . ' WHERE id = :id');
        $sql->execute(array (
            ':id' => $id,
        ));

        $resultSet = $sql->fetch();
        return $resultSet;
    }

    /**
     * SELECT clause for gORM. This simply returns a SELECT string but is nonetheless needed to keep the way this system
     * works consistent across the framework.
     * @return string
     */
    public function select()
    {
        return('SELECT ');
    }

    /**
     * Construct the FROM DB clause for the gORM
     * @param array $tableName
     * @param array $columns
     * @return string
     */
    public function from(array $tableName, array $columns)
    {
        foreach ($tableName as $key => $value)
        {
            $tableSql = $value . ' AS ' . $key;
        }

        $columnSql = '';
        foreach ($columns as $column)
        {
            $columnSql .= $column . ', ';
        }

        $columnSql = substr($columnSql, 0, -2);

        $fromSql = $columnSql . ' FROM ' . $tableSql;

        return $fromSql;
    }

    public function innerJoin(array $joinTable, $joinColumn)
    {
        foreach ($joinTable as $key => $value)
        {
            $joinTableSql = ' INNER JOIN ' . $value . ' AS ' . $key;
        }

        $innerJoinSql = $joinTableSql . ' ON ' . $joinColumn;

        return $innerJoinSql;
    }

    public function where($where)
    {
        $whereSql = ' WHERE ' . $where;

        return $whereSql;
    }

    public function andWhere($andWhere)
    {
        $andWhereSql = ' AND WHERE ' . $andWhere;

        return $andWhereSql;
    }

}